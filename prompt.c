#define _GNU_SOURCE
#define _POSIX_C_SOURCE 2

#include <unistd.h>

#include "vcs.h"
#include "prompt.h"

#ifdef T
#include "t.h"
#endif


int main(int argc, char** argv){
    // Exit if no previous exit status supplied
    if(argc != 2) return 127;

    /* Maximum username length on Linux is 32
     * Typically 64 byte are enough for Linux hostnames, even though SUSv2
     * limits to 255 bytes */
    char status[32], userhost[128], wd[1024], symbol[72], vcs_status[64],
         vim_status[32];
    void *cwd = getcwd(NULL, 0);


    prompt_format_exitcode(status, argv[1]);
    prompt_user_host(userhost, 128);
    prompt_format_cwd(wd, (const char*) cwd, 1024);
    prompt_format_vcs(vcs_status, (const char*) cwd, 64);

    /* Flushing after the first line seems to fix the random hangup of the
     * prompt. Might be necessary after each line of the prompt */
    fflush(stdout);

    prompt_cmd_symbol(symbol, 72);
    prompt_viminfo(vim_status, 32);

    printf("%s%s %s %s\n%s %s", status, userhost, wd, vcs_status,
           symbol, vim_status);
    fflush(stdout);

    free(cwd);

    return 0;
}
