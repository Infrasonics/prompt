#ifndef __PROMPT_H
#define __PROMPT_H

#define _GNU_SOURCE

#define SIZE_USERNAME 32
#define SIZE_HOSTNAME 64

#ifdef ZSH
#define SIZE_COLORCODE 12
#else
#define SIZE_COLORCODE 12
#endif


typedef struct _COLOR {
   char BLACK[16];
   char BOLD_BLACK[16];
   char RED[16];
   char BOLD_RED[16];
   char GREEN[16];
   char BOLD_GREEN[16];
   char YELLOW[16];
   char BOLD_YELLOW[16];
   char BLUE[16];
   char BOLD_BLUE[16];
   char PURPLE[16];
   char BOLD_PURPLE[16];
   char CYAN[16];
   char BOLD_CYAN[16];
   char WHITE[16];
   char BOLD_WHITE[16];
   char END[16];
} COLOR;


#ifdef ZSH
static const COLOR color = {
   "%{\033[00;30m%}",
   "%{\033[01;30m%}",
   "%{\033[00;31m%}",
   "%{\033[01;31m%}",
   "%{\033[00;32m%}",
   "%{\033[01;32m%}",
   "%{\033[00;33m%}",
   "%{\033[01;33m%}",
   "%{\033[00;34m%}",
   "%{\033[01;34m%}",
   "%{\033[00;35m%}",
   "%{\033[01;35m%}",
   "%{\033[00;36m%}",
   "%{\033[01;36m%}",
   "%{\033[00;37m%}",
   "%{\033[01;37m%}",
   "%{\033[0m%}"
};
#else
static const COLOR color = {
   "\033[00;30m",
   "\033[01;30m",
   "\033[00;31m",
   "\033[01;31m",
   "\033[00;32m",
   "\033[01;32m",
   "\033[00;33m",
   "\033[01;33m",
   "\033[00;34m",
   "\033[01;34m",
   "\033[00;35m",
   "\033[01;35m",
   "\033[00;36m",
   "\033[01;36m",
   "\033[00;37m",
   "\033[01;37m",
   "\033[0m"
};
#endif

/* Colors used for different parts of the prompt */
#define COLROOT  color.RED          // root symbol
#define COLUSER  color.BOLD_CYAN    // user symbol
#define COLEXITC color.YELLOW       // exit code
#define COLHOST  color.GREEN        // hostname
#define COLPWD   color.BOLD_BLUE    // current working dir
#define COLVIM   color.PURPLE       // vim indicator
#define COLGIT   color.YELLOW       // git status
#define COLPIJUL color.BOLD_GREEN
#define COLMERCURIAL color.BOLD_WHITE

/* Symbols for the different VCSs */
#define SYMGIT "\ue0a0"
#define SYMPIJUL "\u2388"
#define SYMHG "\u263f"

int prompt_ssh_connected();
void prompt_format_exitcode(char*, char*);
void prompt_user_host(char*, size_t);
void prompt_cmd_symbol(char*, size_t);
void prompt_format_cwd(char*, const char*, size_t);
void prompt_viminfo(char*, size_t);
void prompt_format_vcs(char*, const char*, size_t);


#endif
