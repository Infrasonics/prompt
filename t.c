#include <stdio.h>
#include "t.h"

/* Checks for existence of a specified todo file */
int
t_tf_exists(const char *todofile)
{
    struct stat s;
    stat(todofile, &s);
    if(S_ISREG(s.st_mode)) {
        return 1;
    } else {
        return 0;
    }
}

/* retrieve the number of tasks left in the todo file */
int
t_getnumtodos(const char *todofile)
{
    if(t_tf_exists(todofile)) {
        int c=0;
        char *line;
        size_t len = 0;
        FILE *tstream = fopen(todofile, "r");
        if(tstream == NULL) exit(EXIT_FAILURE);

        // Simply count lines in todo file
        while(getline(&line,&len,tstream) != -1) {
            c++;
        }

        fclose(tstream);
        return c;
    } else {
        return 0;
    }
}
