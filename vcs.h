#ifndef __VCS_H
#define __VCS_H

#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <stdlib.h>
#include <assert.h>

#include "path.h"

int git_get_current_branch(const char*, char*, size_t);
int pijul_get_current_branch(char*, size_t);
int vcs_check_vcs_above(const char*, char*, const char*, size_t);

enum {
    GITSTATUS_NORMAL = 0,
    GITSTATUS_DETACHED_HEAD = 1,
    GITSTATUS_DIRTY = 2,
    GITSTATUS_MODIFIED = 4
};

#endif
