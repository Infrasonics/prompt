CC=gcc
CFLAGS= -std=gnu99 -Wall -Wextra -Os -pedantic -DZSH -DT
EXEC=zsh_prompt
SRC=prompt.c libprompt.c vcs.c path.c t.c
OBJ=$(SRC:%.c=%.o)

$(EXEC): $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) -o $(EXEC)

archive: gitlib
	ar -rcs libgit.a git.o
	ar -rcs libprompt.a libprompt.c

install: $(EXEC)
	cp $(EXEC) $${HOME}/usr/bin

clean:
	rm -f ./*.o

purge: clean
	rm -f $(EXEC)
