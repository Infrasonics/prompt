# Everyday zsh/bash prompt

I wanted to share my trusty prompt in the hope it might be useful to somebody
else as well. This started out as a python script (which is also included)
but was too slow on my old hardware, so it received a rewrite in C. Maybe all
of this can be done in bash natively too; I can't remember the exact reason I
started tinkering.


## Features
- Reminds you what VCS is used in the current directory.
- Shows hostname of the remote machine only if `ssh`ing in.
- Has a different prompt symbol when run as root.
- Support for Steve Losh's [`t`](https://stevelosh.com/projects/t/), at least
  shows how many things are left to do xD.
- Shows exit status of last command if non-zero


## Issues
- So far only detects `git`, `pijul`, and mercurial. Easy to extend.
- Only checks for a `ssh` connection via environment variable, might not catch
  all cases.


## Configuration
The configuration is not neatly put in a single file as of now, places that
might be interesting for smaller configurations include:
- `prompt.h` for colors and VCS symbols
- `t.h` (you most probably want to adapt the path to your todo list) if you're
  using it


## Building
Adapt the `Makefile` to suit your needs. Of particular interest might be the
- `-DT` to add some code checking the length of your `t`-todo file and
- `-DZSH` which changes the color escape sequences from bash to zsh
flags.

## Usage
Configure your `PS1` to use the program. E.g. for `zsh`:
```
precmd(){
PS1="$(/path/to/zsh_prompt $?)"
}
```
`$?` to get the exit code of the last command
