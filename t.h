#ifndef __T_H
#define __T_H

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>

#define TFILEPATH "/home/f11/.gcal/todo.txt"

int t_tf_exists(const char *todofile);
int t_getnumtodos(const char *todofile);

#endif
