#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pwd.h>

#include "prompt.h"
#include "vcs.h"

#ifdef T
#include "t.h"
#endif

void
prompt_format_exitcode(char *buf, char *code)
{
    if(atoi(code) == 0) {
        strncpy(buf, "", 1);
    } else {
        memcpy(buf, COLEXITC, SIZE_COLORCODE);
        strncpy(buf+SIZE_COLORCODE, code, 4);
        char *npos = strchr(buf, '\0');
        *npos = ' ';
        strncpy(npos+1, color.END, SIZE_COLORCODE);
    }
}


int
prompt_ssh_connected()
{
    if(getenv("SSH_CLIENT") != NULL || getenv("SSH_TTY") != NULL) {
        return 1;
    } else {
        pid_t parent = getppid();
        char ppid_path[24];
        if(snprintf(ppid_path, 24, "/proc/%d/cmdline", parent) <0) {
            return 0;
        }
        FILE *cmdline = fopen(ppid_path, "r");

        // Reuse buffer
        if(fgets(ppid_path, 4, cmdline) == NULL) {
            return 0;
        }
        fclose(cmdline);

        if(strncmp(ppid_path, "sshd", 4) == 0) {
            return 1;
        }
    }

    return 0;
}


void
prompt_user_host(char *buf, size_t len)
{
    char user[SIZE_USERNAME], hostname[SIZE_HOSTNAME+1];
    struct passwd *pwu = getpwuid(getuid());
    if(pwu == NULL) {
        strncpy(user, "anonymous", 10);
    } else {
        strncpy(user, pwu->pw_name, SIZE_USERNAME);
    }

    if(prompt_ssh_connected()) {
        // Leave one byte empty for the (at) later
        if(gethostname(hostname+1, SIZE_HOSTNAME-1) != 0) {
            strncpy(hostname+1, "unknown", 8);
        }
        // Make sure the string is null-terminated, even when truncated
        hostname[0] = '@';
        hostname[SIZE_HOSTNAME] = '\0';

    } else {
        strncpy(hostname, "", 1);
    }

    snprintf(buf, len, "%s%s%s%s", COLHOST, user, hostname, color.END);
}


void
prompt_cmd_symbol(char *buf, size_t len)
{
#ifdef T
        size_t c = t_getnumtodos(TFILEPATH);
#endif
    if(getuid() == 0) {
/* If we do have something in the todo list show the number of todos instead of
 * the prompt symbol */
#ifdef T
        if (c > 0) {
            snprintf(buf, len, "%s✗%ld%s", COLROOT, c, color.END);
        } else {
#endif
        // standard symbol when there are no tasks in t
        snprintf(buf, len, "%s%s%s", COLROOT, "α", color.END);
#ifdef T
        }
#endif
    } else {
/* If we do have something in the todo list show the number of todos instead of
 * the prompt symbol */
#ifdef T
        if (c > 0) {
            snprintf(buf, len, "%s[%ld]%s", COLUSER, c, color.END);
        } else {
#endif
        snprintf(buf, len, "%s%s%s", COLUSER, "Z%#", color.END);
#ifdef T
        }
#endif
    }
}


//TODO check for case when HOME is exported as /home/user/ (with trailing slash
void
prompt_format_cwd(char *buf, const char *cwd, size_t len)
{
    char* home = getenv("HOME");

    if(strstr(cwd, home) == cwd) {
        if(strlen(cwd) == strlen(home)) {
            // We're in $HOME
            snprintf(buf, len, "%s%s%s",
                     COLPWD, "~", color.END);
            return;
        }
        snprintf(buf, len, "%s%s%s",
                 COLPWD, cwd+strlen(home)-1, color.END);
        /* Find the first separator and replace the byte before that
         * with the tilde */
        char* sep = strchr(buf, '/');
        *(sep-1) = '~';
    } else {
        snprintf(buf, len, "%s%s%s",
                 COLPWD, cwd, color.END);
    }
}


/* Show a prompt indicator of "(vim)" when calling terminal from inside vim.
 * Maybe not too useful when using neovim and its terminal */
void
prompt_viminfo(char *buf, size_t len)
{
    if(getenv("VIMRUNTIME") != NULL) {
        strncpy(buf, COLVIM, len);
        strncpy(buf+SIZE_COLORCODE, "(vim) ", len-SIZE_COLORCODE);
        strncpy(buf+SIZE_COLORCODE+6, color.END, len-6-SIZE_COLORCODE);
    } else {
        strncpy(buf, "\0", 1);
    }
}


void
prompt_format_git(char *buf, const char *gitdir, size_t len)
{
    char branchname[32], descriptors[32];
    int status = git_get_current_branch(gitdir, branchname, 32);
    switch(status) {
        case GITSTATUS_NORMAL:
            snprintf(descriptors, 1, "%s", "\0");
            break;
        case GITSTATUS_DETACHED_HEAD:
            snprintf(descriptors, 16, "%s", "(detached HEAD)");
            break;
    }
    snprintf(buf, len, "%s%s %s %s%s", COLGIT,
             SYMGIT, branchname, descriptors,
             color.END);
}


void
prompt_format_pijul(char *buf, size_t len)
{
    char branchname[32], descriptors[32];
    pijul_get_current_branch(branchname, 32);
    strncpy(descriptors, "\0", 1);
    snprintf(buf, len, "%s%s  %s %s%s", COLPIJUL,
             SYMPIJUL, branchname, descriptors,
             color.END);
}

//TODO expand to make usable
void
prompt_format_mercurial(char *buf, size_t len)
{
    char branchname[32], descriptors[32];
    strncpy(branchname, "\0", 1);
    strncpy(descriptors, "\0", 1);
    snprintf(buf, len, "%s%s  %s %s%s", COLMERCURIAL,
             SYMHG, branchname, descriptors,
             color.END);
}

//XXX FCFS, maybe add an indicator to show multiple VCSs warning
void
prompt_format_vcs(char *buf, const char *cwd, size_t len)
{
    char dir[256];
    if(vcs_check_vcs_above(cwd, dir, "git", 256)) {
        prompt_format_git(buf, dir, len);
    } else if(vcs_check_vcs_above(cwd, dir, "pijul", 256)) {
        prompt_format_pijul(buf, len);
    } else if(vcs_check_vcs_above(cwd, dir, "mercurial", 256)) {
        prompt_format_mercurial(buf, len);
    } else {
        strncpy(buf, "", len > 1 ? 1 : len);
        return;
    }
}

