#include "vcs.h"
#include <alloca.h>
#include <stdio.h>

#define MAX(x, y) __extension__ ( { \
    typeof(x) _max1 = (x);          \
    typeof(y) _max2 = (y);          \
    (void) (&_max1 == &_max2);      \
    _max1 > _max2 ? _max1 : _max2; })

#define MAXDEPTH_GIT 8


int __attribute__((visibility("hidden")))
selector_dotgit(const struct dirent *dir)
{
    if(strcmp(dir->d_name, ".git") == 0) {
        return 1;
    }
    return 0;
}


int __attribute__((visibility("hidden")))
selector_dotpijul(const struct dirent *dir)
{
    if(strcmp(dir->d_name, ".pijul") == 0) {
        return 1;
    }
    return 0;
}

int __attribute__((visibility("hidden")))
selector_dothg(const struct dirent *dir)
{
    if(strcmp(dir->d_name, ".hg") == 0) {
        return 1;
    }
    return 0;
}


int
vcs_check_vcs_above(const char *cwd, char* vcsparentdir, const char* vcs, size_t len)
{
    size_t size_cwd = strlen(cwd) * sizeof(char);
    char *dir = alloca(size_cwd);
    memcpy(dir, cwd, size_cwd);
    *(dir+size_cwd) = '\0';

    int (*selector)(const struct dirent *) = NULL;
    if(strcmp(vcs, "git") == 0) {
        selector = selector_dotgit;
    } else if(strcmp(vcs, "pijul") == 0) {
        selector = selector_dotpijul;
    } else if(strcmp(vcs, "mercurial") == 0) {
        selector = selector_dothg;
    } else {
        /* make sure the selector is set to something useful */
        selector = selector_dotgit;
    }

    int res = 0;
    char *nul;
    for(int i=0;i<MAXDEPTH_GIT;i++) {
        struct dirent **namelist;
        /* Making sure the null byte is properly set after the call to scandir,
         * because scandir does alter dir, a weird bug is assumed */
        nul = strchr(dir, '\0');
        int n = scandir(dir, &namelist, selector, NULL);
        *nul = '\0';
        res = MAX(n, res);
        if(n > 0) {
            strncpy(vcsparentdir, dir, len);
        } else if(n == 0) {
            dir = path_dirname(dir);
        }
        if(n >= 0) {
            while(n--) {
                free(namelist[n]);
            }
            free(namelist);
        } else {
            // Ignore errors
            return res > 0 ? 1 : 0;
        }
    }
    return res > 0 ? 1 : 0;
}


int
git_get_current_branch(const char *gitparentdir, char *buf, size_t len)
{
    char path2head[strlen(gitparentdir)+32];
    memcpy(path2head, gitparentdir, strlen(gitparentdir));
    memcpy(path2head+strlen(gitparentdir), "/.git", 5);
    memcpy(path2head+strlen(gitparentdir)+5, "/HEAD\0", 6);

    FILE *head = fopen(path2head, "r");
    if(head != NULL) {
        if(fgets(path2head, strlen(gitparentdir)+32, head) == NULL) {
            return -1;
        }
    } else {
        return -1;
    }

    fclose(head);

    char *lsep = strrchr(path2head, '/');

    if(lsep == NULL) {
        // No slash means detached HEAD state, so only a hash in the file
        // Show only first 8 chars of hash
        strncpy(buf, path2head, 8);
        buf[8] = '\0';
        return GITSTATUS_DETACHED_HEAD;

    } else {
        /* Discard the newline read from the file */
        char *nl = strrchr(path2head, '\n');
        *nl = '\0';

        assert(nl-lsep > 0);
        strncpy(buf, lsep+1, len>=(size_t)(nl-lsep) ? (size_t)(nl-lsep) : len);
    }

    return GITSTATUS_NORMAL;
}


int
pijul_get_current_branch(char *buf, size_t len)
{
    FILE *fp;
    char line[64];
    fp = popen("pijul branches", "r");
    if(fp == NULL) {
        strncpy(buf, "\0", len);
    } else {
        char *s = fgets(line, sizeof(line), fp);
        // No previous call to `pijul fork`, so no branches output
        if (s == NULL) {
            strncpy(buf, "\0", len);
        } else {
            do {
                if(strncmp(line, "*", 1) == 0) {
                    strncpy( buf, (line + 2), len);
                    // Strip the trailing newline
                    char *nl = strstr(buf, "\n");
                    *nl = '\0';
                } else continue;
            } while(fgets(line, sizeof(line), fp) != NULL);
        }
    }
    pclose(fp);
    //TODO state of the repository
    return 0;
}
