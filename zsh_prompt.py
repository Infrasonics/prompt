#!/usr/bin/python3

import os
import re
import sys
import socket
import getpass
import subprocess


class Color:
   #  Include a string as a literal escape sequence. The string within the
   #  braces should not change the cursor position. Brace pairs can nest.
   red         = '%{\033[00;31m%}'
   bold_red    = '%{\033[01;31m%}'
   green       = '%{\033[00;32m%}'
   bold_green  = '%{\033[01;32m%}'
   yellow      = '%{\033[00;33m%}'
   bold_yellow = '%{\033[01;33m%}'
   blue        = '%{\033[00;34m%}'
   bold_blue   = '%{\033[01;34m%}'
   purple      = '%{\033[00;35m%}'
   bold_purple = '%{\033[01;35m%}'
   cyan        = '%{\033[00;36m%}'
   bold_cyan   = '%{\033[01;36m%}'
   lightgrey   = '%{\033[00;37m%}'
   white       = '%{\033[01;37m%}'
   end         = '%{\033[0m%}'



class Prompt:
   def __init__(self, exitCode):
      self.exitCode = exitCode
      self.user  = getpass.getuser()
      self.host  = socket.gethostname()
      self.inSSH = self.checkForSSHCon()
      self.inVIM = self.checkForVIM()
      self.curDir = os.environ['PWD']
      self.home = os.environ['HOME']
      self.rootSymbol = Color.red + 'α' + Color.end
      self.userSymbol = Color.bold_cyan + 'Z%#' + Color.end
      self.vimIndicator = Color.purple + ' (vim)' + Color.end
      self.todoIndicator = Color.bold_cyan + '[' + str(self.getTodo()) + ']' + Color.end
      self.commandSymbol = self.getCommandSymbol()


   def checkForSSHCon(self):
      try:
         os.environ['SSH_CONNECTION']
         return True
      except KeyError:
         return False


   def checkForVIM(self):
      try:
         os.environ['VIMRUNTIME']
         return True
      except KeyError:
         return False


   def getCommandSymbol(self):
      symbol = ''
      if self.getTodo() > 0:
         if self.user == 'root':
            symbol += Color.red + '✗' + str(self.getTodo()) + Color.end
         else:
            symbol += self.todoIndicator
      else:
         if self.user == 'root':
            symbol = self.rootSymbol
         else:
            symbol = self.userSymbol
      if self.checkForVIM():
         symbol += self.vimIndicator

      return symbol


   def showPWD(self, maxdepth):
      pwd = self.curDir
      if self.curDir.find(self.home) == 0:
         pwd = pwd.replace(self.home, '~', 1)
      names = pwd.split(os.sep)
      if len(names) > maxdepth:
         names = names[:2] + ['<..>'] + names[2 - maxdepth:]
         pwd = os.sep.join(names)
      pwd = Color.bold_blue + pwd + Color.end
      return pwd


   def showExitCode(self):
      if int(self.exitCode) == 0:
         return ''
      else:
         status  = Color.yellow
         status += str(self.exitCode) + ' '
         status += Color.end
         return status


   def showUserHost(self):
      user   = Color.green
      user  += self.user
      if self.inSSH:
         user += '@' + self.host
      user += Color.end
      return user


   def showGitStatus(self):
      gitStatus = ''
      try:
         subprocess.check_call(['git', 'rev-parse', '--is-inside-work-tree'],
            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
         branch = subprocess.check_output(['git', 'branch', '--no-color'],
            stderr = subprocess.DEVNULL).decode('utf-8').strip('* \n')
         # Only display current branch
         branches = branch.split('\n')
         try:
            if [b.lstrip(' *') for b in branches if b.startswith('*')]:
               branch = [b.lstrip(' *') for b in branches if b.startswith('*')][0]
            else:
               branch = branch.split('\n')[0]
               branch = branch.split('\n')[0]
         except IndexError:
            pass

         if subprocess.check_output(['git', 'ls-files', '.',
            '--exclude-standard', '--others']).decode('utf-8') != '':
            dirty = True
         else: dirty = False

         try:
            subprocess.check_call(['git', 'diff', '--quiet'])
            try:
               subprocess.check_call(['git', 'diff', '--cached', '--quiet'])
               modified = False
            except subprocess.CalledProcessError:
               modified = True
         except subprocess.CalledProcessError:
            modified = True

         gitStatus += '\ue0a0 ' + branch
         if dirty or modified:
            gitStatus += ' |'

         if dirty:
            gitStatus += ' dirty'

         if modified:
            if dirty: gitStatus += ','
            else: gitStatus += ' '
            gitStatus += 'modified'

         #TODO rebase
         return Color.yellow + gitStatus + Color.end

      except subprocess.CalledProcessError: # => not in git repository
         return ''
      except FileNotFoundError: # git not installed
         return ''


   def getTodo(self):
      try:
        import t
      except ImportError:
        return 0
      tododir = os.path.join(self.home, '.gcal')
      todofile = 'todo.txt'
      if os.path.isfile(os.path.join(tododir, todofile)):
         opts = {'finish': None, 'grep': '', 'name': todofile, 'edit': '', 'quiet':
         False, 'remove': None, 'done': False, 'verbose': False, 'taskdir':
         tododir, 'delete': True}
         args = []

         return t.wrapper_main(opts, args)
      else:
         return 0


   def __str__(self):
      prompt = ''
      prompt += self.showExitCode()
      prompt += self.showUserHost()
      prompt += ' '
      prompt += self.showPWD(6)
      prompt += ' '
      prompt += self.showGitStatus()
      prompt += '\n'
      prompt += self.commandSymbol
      prompt += ' '
      return prompt



def main():
   p = Prompt(sys.argv[1])
   print(str(p), end='', flush=False)


if __name__ == "__main__": main()
